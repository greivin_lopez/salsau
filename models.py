#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Module: models

Contains all data models that will be used within the system.
'''
def validate_string(string):
	"""Validates that the provided argument is a string or unicode object
	If the provided value is in fact a string nothing will happen otherwise
	a TypeError is raised
	"""
	if not isinstance(string, (str, unicode)):
		raise TypeError("Expected object of type string, got {}".
			format(type(string).__name__))

class Person(object):
	"""Abstract representation of a person"""
	def __init__(self, id, first_name, last_name):
		validate_string(first_name)
		validate_string(last_name)
		self.__id = id	
		self.__first_name = first_name
		self.__last_name = last_name

	def __str__(self):
		return u"Id: %s - Name: %s %s" % (self.__id, self.__first_name, self.__last_name)

	@property
	def id(self):
		return self.__id

	@property
	def first_name(self):
		return self.__first_name

	@property
	def last_name(self):
		return self.__last_name


class Student(Person):
	"""Represents a student person"""
	def __init__(self, id, first_name, last_name, address):
		Person.__init__(self, id, first_name, last_name)
		self.__address = address

	@property
	def address(self):
		return self.__address


class Teacher(Person):
	"""Represents a teacher person"""
	def __init__(self, id, first_name, last_name):
		Person.__init__(self, id, first_name, last_name)


class Course(object):
	"""Represents a course"""
	# List of valid course names
	course_names = ['Math', 'Science', 'History']
	
	def __init__(self, id, name, description, capacity):
		validate_string(name)
		validate_string(description)
		if not name in self.course_names:
			raise ValueError('Invalid course name received')
		if not isinstance(capacity, int):
			raise TypeError("Expected object of type integer, got {}".
			format(type(capacity).__name__))
		if not capacity > 0:
			raise ValueError('Capacity value must be an integer bigger than zero')
		self.__id = id
		self.__name = name
		self.__description = description
		self.__capacity = capacity
		self.__students = {}
		self.__teacher = None

	@property
	def id(self):
		return self.__id

	@property
	def name(self):
		return self.__name

	@property
	def description(self):
		return self.__description

	@property
	def capacity(self):
		return self.__capacity

	@property
	def teacher(self):
		return self.__teacher

	@teacher.setter
	def teacher(self, teacher):
		if self.__teacher is None:
			self.__teacher = teacher

	@property
	def students_count(self):
		return len(self.__students)

	@property
	def seats_available(self):
		return self.__capacity - len(self.__students)

	def already_registered(self, student):
		return (student.id in self.__students.keys())

	def register_student(self, student):
		self.__students[student.id] = student

	def __str__(self):
		return 'Course: %s - Id: %s' % (self.__name, self.__id)

	def print_students(self):
		return u"".join([u"".join(["    * ", str(x), "\n"]) for x in self.__students.values()])

	def __print(self, text):
		return text if text is not None else ""

	def print_course(self):
		text = u"""
  Course: %s
  Id: %s
  Description: %s
  Teacher: %s
  Students: 
%s

""" % (self.__print(self.__name),
	   self.__print(self.__id),
	   self.__print(self.__description),
	   self.__print(str(self.__teacher) if self.__teacher is not None else ""),
	   self.__print(self.print_students()))
		return text




