#!/usr/bin/env python
# -*- coding: utf8 -*-
import models
import controllers
import unittest

class TestModels(unittest.TestCase):

    def test_not_string_for_person_name(self):
        self.assertRaises(TypeError, models.Person, 69)
        self.assertRaises(TypeError, models.Person, "1", "Greivin", [])

    def test_not_string_for_course_name(self):
        self.assertRaises(TypeError, models.Course, "1", 42, "", 5)
        self.assertRaises(TypeError, models.Course, "2", {}, "description", 5)

    def test_not_valid_course_name(self):
        self.assertRaises(ValueError, models.Course, "1", "Modern Art", "", 5)

    def test_not_valid_capacity(self):
        self.assertRaises(TypeError, models.Course, "1", "Science", "", "Not Valid")
        self.assertRaises(ValueError, models.Course, "1", "Science", "", -255)
        self.assertRaises(ValueError, models.Course, "1", "Science", "", 0)

    def test_basic_person_constructor(self):
        bob = models.Person("1", "Bob", "Smith")
        self.assertEqual(bob.first_name, "Bob")
        self.assertEqual(str(bob), "Id: 1 - Name: Bob Smith")

    def test_teacher_constructor(self):
        self.assertRaises(TypeError, models.Teacher, "1", "Greivin", [])
        self.assertRaises(TypeError, models.Teacher, "1", 5, u"López")
        ryan = models.Teacher("1", "Ryan", "Doe")
        self.assertEqual(str(ryan), "Id: 1 - Name: Ryan Doe")

    def test_student_constructor(self):
        self.assertRaises(TypeError, models.Student, "1", "Greivin", [])
        self.assertRaises(TypeError, models.Student, "1", 5, u"López")
        john = models.Student("1", "John", "Doe", "1234 Main Street. Anytown, USA. 123456")
        self.assertEqual(str(john), "Id: 1 - Name: John Doe")

    def test_basic_course_constructor(self):
        science = models.Course("101", "Science", "Introduction to Science", 20)
        self.assertEqual(science.id, "101")
        self.assertEqual(science.name, "Science")
        self.assertEqual(science.description, "Introduction to Science")
        self.assertEqual(science.capacity, 20)
        self.assertEqual(science.teacher, None)
        self.assertEqual(science.students_count, 0)
        self.assertEqual(science.seats_available, 20)
        self.assertEqual(str(science), "Course: Science - Id: 101")

    def test_enrollment_manager(self):
        manager = controllers.EnrollmentManager()
        self.assertDictEqual({}, manager.students)
        self.assertDictEqual({}, manager.courses)
        self.assertDictEqual({}, manager.teachers)
        
        manager.create_student("1", "John", "Doe", "1234 Main Street. Anytown, USA. 123456")
        self.assertTrue(len(manager.students) > 0)
        
        manager.create_course("101", "Science", "Introduction to Science", 1)
        self.assertTrue(len(manager.courses) > 0)
        
        manager.create_teacher("2", "Ryan", "Doe")
        self.assertTrue(len(manager.teachers) > 0)
        
        self.assertRaises(ValueError, manager.get_student, "42")
        student = manager.get_student("1")
        self.assertEqual(student.first_name, "John")
        self.assertEqual(student.last_name, "Doe")
        self.assertEqual(student.address, "1234 Main Street. Anytown, USA. 123456")

        self.assertRaises(ValueError, manager.get_course, "42")
        course = manager.get_course("101")
        self.assertEqual(course.name, "Science")
        self.assertEqual(course.description, "Introduction to Science")
        self.assertEqual(course.capacity, 1)

        self.assertRaises(ValueError, manager.get_teacher, "42")
        teacher = manager.get_teacher("2")
        self.assertEqual(teacher.first_name, "Ryan")
        self.assertEqual(teacher.last_name, "Doe")

        self.assertRaises(ValueError, manager.register_student, "42", "101")
        self.assertRaises(ValueError, manager.register_student, "1", "69")

        # Register the student to Science course
        manager.register_student("1", "101")
        self.assertEqual(course.seats_available, 0)
        course = manager.get_course("101")
        self.assertEqual(course.students_count, 1)
        # Testing value error on already registered users
        self.assertRaises(ValueError, manager.register_student, "1", "101")
        # Testing full class error
        manager.create_student("2", "Juan", "Chavez", "1234 Main Street. Anytown, USA. 123456")
        self.assertRaises(controllers.FullClassException, manager.register_student, "2", "101")

        self.assertRaises(ValueError, manager.assign_teacher, "42", "101")
        self.assertRaises(ValueError, manager.assign_teacher, "2", "69")
        # Assign teacher to Science course
        manager.assign_teacher("2", "101")
        # Testing already assigned teacher
        self.assertRaises(ValueError, manager.assign_teacher, "2", "101")


if __name__ == '__main__':
    unittest.main()