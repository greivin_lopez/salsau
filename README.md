# _SalsaU_

_Description:_ Salsamobi University Enrollment System. This is a small CLI prove of concept to let an user to create: Courses, Teachers and Students and to manage the students enrollment to the courses. It is a suggested solution to a programming interview. 

## Technology Used

_The solution was created using plain Python v2.7.7. It is a CLI created using the [curses library](https://docs.python.org/2/howto/curses.html)_ 

## Testing

_How do I run the project's automated tests?_

### Unit Tests

For unit testing navigate to the code folder and run the next command:

`python tests.py`


## Run Program

_How do I run the program?_

Open a terminal (console), navigate to the code folder and run the next command:

`python salsau.py`


You will see a screen like this:


![Home menu](http://content.screencast.com/users/GreivinLopez/folders/Jing/media/2ababceb-fbeb-4978-a014-9aaee441c722/00000077.png)

From there just follow the program instructions.

To quit the program you can hit *q* or *7* keys on main menu or press *Ctrl-C* on any screen.
 