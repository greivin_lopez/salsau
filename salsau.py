#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Salsamobi University course enrollment system.

To run the program:
	$> python salsau.py

Another option on UNIX based systems:
    $> sudo chmod u=rx salsau.py
    $> ./salsau

@author:     Greivin López
            
@copyright:  2014 Salsamobi. All rights reserved.
            
@license:    Commercial

@contact:    greivin.lopez@gmail.com
'''
import argparse
import os
import sys
import views


__version__ = 0.1
__date__ = '2014-6-9'
__updated__ = '2014-6-9'


def main(argv=None):
    '''Main function. The entry point of the system.'''
    # Output system information
    program_name = os.path.basename(sys.argv[0])
    program_version = "%s" % __version__
    program_build_date = "%s" % __updated__
 
    program_version_string = '%%prog %s (%s)' % (program_version, program_build_date)
    program_description = "Welcome to Salsamobi University."
    program_license = u"Copyright 2014 Salsamobi. All rights reserved."

    try:
        # 
        # Command line parser is not required for this project but I designed it
        # like this to allow future usage of command line arguments
        # 
        # Setup command line parser
        parser = argparse.ArgumentParser(
        	description=program_description,
        	epilog=program_license)
        
        # Process arguments
        args = parser.parse_args()
        
        # DISPLAY MAIN MENU #
        views.landing_view()
        sys.exit(0);
        
    except Exception, e:
        print "There was an unexpected error: {}".format(e)
        sys.exit(1)


if __name__ == "__main__": main()
