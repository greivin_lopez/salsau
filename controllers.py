#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Module: controllers.

Contains manager classes that will handle the core functionality and will be the glue
between views and models.

The EnrollmentManager class is an example of a "classic singleton" as described in the
book Learning Python Design Patterns by Gennadiy Zlobin:
http://www.packtpub.com/learning-python-design-patterns/book
Chapter 2, Page 19.
'''
import models

class FullClassException(Exception):
	"""Exception thrown when a class have not seats available"""
	pass


class EnrollmentManager(object):
	"""Manages the core logic of the system by interfacing models with views"""
	courses = {}
	students = {}
	teachers = {}

	def __new__(cls):
		"""Ensures only one instance of the object is instantiated"""
		if not hasattr(cls, 'instance'):
			cls.instance = super(EnrollmentManager, cls).__new__(cls)
		return cls.instance

	def create_student(self, id, first_name, last_name, address):
		if id in self.students.keys():
			raise ValueError("The student was already created")
		# Create the student object
		student = models.Student(id, first_name, last_name, address)
		# Save the student
		self.students[student.id] = student

	def create_course(self, id, name, description, capacity):
		if id in self.courses.keys():
			raise ValueError("The course was already created")
		# Create the course object
		course = models.Course(id, name, description, capacity)
		# Save the course
		self.courses[course.id] = course

	def create_teacher(self, id, first_name, last_name):
		if id in self.teachers.keys():
			raise ValueError("The teacher was already created")
		# Create the teacher object
		teacher = models.Teacher(id, first_name, last_name)
		# Save the teacher
		self.teachers[teacher.id] = teacher

	def get_student(self, id):
		if not id in self.students.keys():
			raise ValueError("Student not found")
		return self.students[id]

	def get_teacher(self, id):
		if not id in self.teachers.keys():
			raise ValueError("Teacher not found")
		return self.teachers[id]

	def get_course(self, id):
		if not id in self.courses.keys():
			raise ValueError("Course not found")
		return self.courses[id]

	def register_student(self, student_id, course_id):
		student = self.get_student(student_id)
		course = self.get_course(course_id)
		if course.already_registered(student):
			raise ValueError("The given student was already registered to this course")
		if not course.seats_available:
			raise FullClassException("The course is full")
		# Everything is OK register the student to course
		course.register_student(student)

	def assign_teacher(self, teacher_id, course_id):
		teacher = self.get_teacher(teacher_id)
		course = self.get_course(course_id)
		if course.teacher is not None:
			raise ValueError("The course already had an assigned teacher")
		# Everything is OK assign the teacher to course
		course.teacher = teacher

	def get_report(self):
		if len(self.courses) == 0:
			return "There aren't any courses registered yet.\n"
		return "".join([x.print_course() for x in self.courses.values()])

	def can_create_course(self):
		"""Gets a value indicating if the user is able to create another course or not"""
		return len(self.courses) < 3
	
			

