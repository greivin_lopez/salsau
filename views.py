#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Module: views.

Basically a menu handler using curses: https://docs.python.org/2/library/curses.html
'''
import curses
import controllers

manager = controllers.EnrollmentManager()

def landing_view():
	screen = curses.initscr()
	curses.start_color()
	# Color for errors
	curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
	# Color for success
	curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
	try:
		main_menu(screen)
	except: # catch *all* exceptions
		show_message(screen, "We're sorry. An unexpected error occur.", 1)
	finally:
		# Do a clean up
		curses.endwin()


def get_param(screen, label):
	curses.echo()
	curses.curs_set(1)
	
	screen.clear()
	screen.border(0)
	screen.addstr(2, 2, label)
	screen.refresh()
	input = screen.getstr(4, 2, 60)
	return input

def show_message(screen, label, color=0):
	curses.echo()
	curses.curs_set(0)
	
	screen.clear()
	screen.border(0)
	screen.addstr(2, 2, label, curses.color_pair(color))
	screen.addstr(10, 2, "Press any key to return to main menu...")
	screen.refresh()
	event = screen.getch()

def main_menu(screen):
	curses.noecho()
	curses.curs_set(0)
	screen.keypad(1)

	screen.clear()
	screen.border(0)
	screen.addstr(2, 2, "Select an option...")
	screen.addstr(4, 4, "1 - Create Course")
	screen.addstr(5, 4, "2 - Create Student")
	screen.addstr(6, 4, "3 - Create Teacher")
	screen.addstr(7, 4, "4 - Register Student to Course")
	screen.addstr(8, 4, "5 - Assign Teacher to Course")
	screen.addstr(9, 4, "6 - Print Report")
	screen.addstr(10, 4, "7 - Exit")
	screen.refresh()

	exit = False
	while True:
		event = screen.getch()
		if event == ord("q") or event == ord("7"):
			exit = True
			break
		elif event == ord("1"): 
			create_course(screen)
			break
		elif event == ord("2"):
			create_student(screen)
			break
		elif event == ord("3"):
			create_teacher(screen)
			break
		elif event == ord("4"):
			register_student(screen)
			break
		elif event == ord("5"):
			assign_teacher(screen)
			break			
		elif event == ord("6"): 
			print_report(screen)
			break

	if exit:
		curses.endwin()
	else:
		main_menu(screen)

def create_course(screen):

	# Verify the user is able to create another course
	if not manager.can_create_course():
		show_message(screen, "Can't create more than 3 courses.", color=1)
		return

	curses.noecho()
	curses.curs_set(0)
	screen.keypad(1)

	screen.clear()
	screen.border(0)
	screen.addstr(2, 2, "Select the course you want to create...")
	screen.addstr(4, 4, "1 - Science")
	screen.addstr(5, 4, "2 - Math")
	screen.addstr(6, 4, "3 - History")
	screen.refresh()

	course_name = ""
	while True:
		event = screen.getch()
		if event == ord("1"):
			course_name = "Science"
			break
		elif event == ord("2"):
			course_name = "Math"
			break
		elif event == ord("3"):
			course_name = "History"
			break

	course_description = get_param(screen, "Enter the a description for the course...")
	course_id = get_param(screen, "Enter an id value for the course...")
	course_capacity = get_param(screen, "Enter the amount of students allowed for the course...")

	capacity = 40
	try:
		capacity = int(course_capacity)
		if capacity <= 0:
			raise ValueError("")
	except ValueError, e:
		error = u"Capacity value must be an integer greater than zero."
		show_message(screen, error, color=1)
		return
	try:	
		manager.create_course(course_id, course_name, course_description, capacity)
	except ValueError, e:
		show_message(screen, str(e), color=1)
		return
	show_message(screen, "The course was successfully created.", color=2)
	return

def create_student(screen):
	first_name = get_param(screen, "Enter the student's first name...")
	last_name = get_param(screen, "Enter the student's last name...")
	id = get_param(screen, "Enter an id for the student...")
	address = get_param(screen, "Enter the student's address...")
	try:
		manager.create_student(id, first_name, last_name, address)
	except ValueError, e:
		show_message(screen, str(e), color=1)
		return
	show_message(screen, "The studed was successfully created.", color=2)
	return

def create_teacher(screen):
	first_name = get_param(screen, "Enter the teacher's first name...")
	last_name = get_param(screen, "Enter the teacher's last name...")
	id = get_param(screen, "Enter an id for the teacher...")
	try:
		manager.create_teacher(id, first_name, last_name)
	except ValueError, e:
		show_message(screen, str(e), color=1)
		return
	show_message(screen, "The teacher was successfully created.", color=2)
	return

def choose_from(screen, message, options):
	curses.noecho()
	curses.curs_set(0)
	screen.keypad(1)

	screen.clear()
	screen.border(0)
	screen.addstr(2, 2, message)

	line = 4
	for option in options:
		option_label = "".join([str(line - 3), " - ", str(option)])
		screen.addstr(line, 4, option_label)
		line += 1
	screen.refresh()

	result = None
	while True:
		event = screen.getch()
		index = event - 49 # Adjust the index from the ASCII value
		if index >= 0 and index <= (len(options) - 1):
			return options[index]
			break
	return	


def register_student(screen):
	courses = manager.courses.values()
	if len(courses) == 0:
		show_message(screen, "You have to create a course first.", color=2)
		return

	students = manager.students.values()
	if len(students) == 0:
		show_message(screen, "You have to create a student first.", color=2)
		return

	message = "Select the course you want to register the user to..."
	course = choose_from(screen, message, courses)

	message = "Select the student you want to register to %s.." % str(course)
	student = choose_from(screen, message, students)

	try:
		manager.register_student(student.id, course.id)
	except Exception, e:
		show_message(screen, str(e), color=1)
		return

	show_message(screen, "The student was successfully registed to %s." % course, color=2)	
	return

def assign_teacher(screen):
	courses = manager.courses.values()
	if len(courses) == 0:
		show_message(screen, "You have to create a course first.", color=2)
		return

	teachers = manager.teachers.values()
	if len(teachers) == 0:
		show_message(screen, "You have to create a teacher first.", color=2)
		return

	message = "Select the course you want to assign the teacher to..."
	course = choose_from(screen, message, courses)

	message = "Select the teacher you want to assign to %s.." % str(course)
	teacher = choose_from(screen, message, teachers)

	try:
		manager.assign_teacher(teacher.id, course.id)
	except Exception, e:
		show_message(screen, str(e), color=1)
		return

	show_message(screen, "The teacher was successfully assigned to %s." % course, color=2)	
	return

def print_report(screen):
	curses.noecho()
	curses.curs_set(0)
	screen.keypad(1)

	report = "".join([manager.get_report(), "  Press any key to return to main menu..."])
	screen.clear()
	screen.addstr(2, 2, report)
	screen.refresh()

	event = screen.getch()
	return
